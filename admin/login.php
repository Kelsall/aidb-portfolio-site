<?php
//include config
require_once('../includes/config.php');

//check if logged in
if($user->is_logged_in()){header('Location: admin.php');}
?>

<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

    <!-- Basic Page Needs
  ================================================== -->
    <meta charset="utf-8">
    <title>A T Kelsall</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Metas
  ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
  ================================================== -->
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Play:400, 700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../css/base.css">
    <link rel="stylesheet" href="../css/skeleton.css">
    <link rel="stylesheet" href="../css/layout.css">
    <link rel="stylesheet" href="../js/jqueryLibrary/jquery-ui.min.css">

    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="../images/favicon.ico">
    <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-touch-icon-114x114.png">

    <!-- Script
    ================================================== -->
    <script src="../js/jqueryLibrary/external/jquery/jquery.js"></script>
    <script src="../js/jqueryLibrary/jquery-ui.min.js"></script>
</head>

<body>
<header>
        <h1 id="siteHeader">A T Kelsall</h1>
        <h4 id="accentColour">Software and Web Development</h4>
</header>

<nav>
    <ul>
      <li>
        <a href="../index.php">Home</a>
      </li>
      <li>
        <a href="../about.php">About</a>
      </li>
      <li>
        <a href="../portfolio.php">Portfolio</a>
      </li>
      <li>
        <a href="../blog.php">Blog</a>
      </li>
      <li>
        <a href="../contact.php">Contact</a>
      </li>
    </ul>
</nav>

<div class="container">

<?php
//process login form if submitted
if(isset($_POST['submit'])){

    $username = trim($_POST['username']);
    $password = trim($_POST['password']);
    
    if($user->login($username,$password)){ 

        //logged in return to index page
        header('Location: admin.php');
        exit;
    

    } else {
        $message = '<p class="error">Wrong username or password</p>';
    }

}//end if submit

if(isset($message)){ echo $message; }
?>

  <div class="sixteen columns">
    <br />
    <br />
  </div>

  <div id="contact" class="eight columns">
    <h3 id="accentColour">Log In</h3>
    <br />
    <form action="" method="post">
      Username: <input type="text" name="username" value="" placeholder="Enter your username"><br>
      Password: <input type="password" name="password" value="" placeholder="Enter your password"><br>
      <label></label><input type="submit" name="submit" value="Login"  />
    </form>
  </div>

</div>
</body>
</html>