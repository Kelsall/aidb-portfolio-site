<?php
//include config
require_once('../includes/config.php');

//if not logged in redirect to login page
if(!$user->is_logged_in()){ header('Location: login.php'); }

//show message from add / edit page
if(isset($_GET['delcat'])){ 

    $stmt = $db->prepare('DELETE FROM blog_cats WHERE catID = :catID') ;
    $stmt->execute(array(':catID' => $_GET['delcat']));

    header('Location: category.php?action=deleted');
    exit;
} 

?>

<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

    <!-- Basic Page Needs
  ================================================== -->
    <meta charset="utf-8">
    <title>A T Kelsall</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Metas
  ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
  ================================================== -->
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Play:400, 700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../css/base.css">
    <link rel="stylesheet" href="../css/skeleton.css">
    <link rel="stylesheet" href="../css/layout.css">
    <link rel="stylesheet" href="../js/jqueryLibrary/jquery-ui.min.css">

    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="../images/favicon.ico">
    <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-touch-icon-114x114.png">

    <!-- Script
    ================================================== -->
    <script src="../js/jqueryLibrary/external/jquery/jquery.js"></script>
    <script src="../js/jqueryLibrary/jquery-ui.min.js"></script>
    <script language="JavaScript" type="text/javascript">
    function delpost(id, title)
    {
      if (confirm("Are you sure you want to delete '" + title + "'"))
      {
          window.location.href = 'admin.php?delpost=' + id;
      }
    }
    </script>
</head>

<body>
<header>
        <h1 id="siteHeader">A T Kelsall</h1>
        <h4 id="accentColour">Software and Web Development</h4>
</header>

<nav>
    <ul>
      <li>
        <a href="../index.php">Home</a>
      </li>
      <li>
        <a href="../about.php">About</a>
      </li>
      <li>
        <a href="../portfolio.php">Portfolio</a>
      </li>
      <li>
        <a href="../blog.php">Blog</a>
      </li>
      <li>
        <a href="../contact.php">Contact</a>
      </li>
    </ul>
</nav>

<?php
  //admin menu
  include 'menu.php';
?>

<div class="container">
    <h3>Blog Categories</h3>
		<?php
		    //show add / edit page
		    if(isset($_GET['action'])){ 
		        echo '<h4 id="accentColour">Post '.$_GET['action'].'.</h4>'; 
		    } 
		?>

		<table>
	    <tr>
	        <th>Title</th>
	        <th>Action</th>
	    </tr>
	    <?php
	        try {

	            $stmt = $db->query('SELECT catID, catTitle, catSlug FROM blog_cats ORDER BY catTitle DESC');
	            while($row = $stmt->fetch()){
	                
	                echo '<tr>';
	                echo '<td>'.$row['catTitle'].'</td>';
	                ?>

	                <td>
	                    <a href="edit-categories.php?id=<?php echo $row['catID'];?>">Edit</a> | 
	                    <a href="javascript:delcat('<?php echo $row['catID'];?>','<?php echo $row['catSlug'];?>')">Delete</a>
	                </td>
	                
	                <?php 
	                echo '</tr>';

	            }

	        } catch(PDOException $e) {
	            echo $e->getMessage();
	        }
	    ?>
	    </table>
      <br />
	    <p><a href='add-category.php'>Add Category</a></p>

	</div>

</body>
</html>