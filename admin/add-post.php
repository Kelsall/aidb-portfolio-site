<?php
//include config
require_once('../includes/config.php');

//if not logged in redirect to login page
if(!$user->is_logged_in()){header('Location: login.php');}
?>

<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

    <!-- Basic Page Needs
  ================================================== -->
    <meta charset="utf-8">
    <title>A T Kelsall</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Metas
  ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
  ================================================== -->
  <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Play:400, 700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../css/base.css">
    <link rel="stylesheet" href="../css/skeleton.css">
    <link rel="stylesheet" href="../css/layout.css">
    <link rel="stylesheet" href="../js/jqueryLibrary/jquery-ui.min.css">

    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="../images/favicon.ico">
    <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-touch-icon-114x114.png">

    <!-- Script
    ================================================== -->
    <script src="../js/jqueryLibrary/external/jquery/jquery.js"></script>
    <script src="../js/jqueryLibrary/jquery-ui.min.js"></script>
    <script src="//tinymce.cachefly.net/4.0/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector: "textarea",
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        });
    </script>
</head>

<body>
<header>
        <h1 id="siteHeader">A T Kelsall</h1>
        <h4 id="accentColour">Software and Web Development</h4>
</header>

<nav>
    <ul>
      <li>
        <a href="../index.php">Home</a>
      </li>
      <li>
        <a href="../about.php">About</a>
      </li>
      <li>
        <a href="../portfolio.php">Portfolio</a>
      </li>
      <li>
        <a href="../blog.php">Blog</a>
      </li>
      <li>
        <a href="../contact.php">Contact</a>
      </li>
    </ul>
</nav>

<?php
    //admin menu
    include('menu.php');
?>

<div class="container">
    
<p><a href="./admin.php">Blog Admin Index</a></p>

<h3>Add Post</h3>

<?php
//if form has been submitted process it
if(isset($_POST['submit'])){

    $_POST = array_map( 'stripslashes', $_POST );

    //collect form data
    extract($_POST);

    //very basic validation
    if($postTitle ==''){
        $error[] = 'Please enter the title.';
    }

    if($postDesc ==''){
        $error[] = 'Please enter the description.';
    }

    if($postCont ==''){
        $error[] = 'Please enter the content.';
    }

    if(!isset($error)){

    try {
        //insert into database
        $stmt = $db->prepare('INSERT INTO blog_posts (postTitle,postDesc,postCont,postDate) VALUES (:postTitle, :postDesc, :postCont, :postDate)') ;
        $stmt->execute(array(
            ':postTitle' => $postTitle,
            ':postDesc' => $postDesc,
            ':postCont' => $postCont,
            ':postDate' => date('Y-m-d H:i:s')
        ));

        $postID = $db->lastInsertId();
        //add categories
        if(is_array($catID)){
            foreach($_POST['catID'] as $catID){
                $stmt = $db->prepare('INSERT INTO blog_post_cats (postID,catID)VALUES(:postID,:catID)');
                $stmt->execute(array(
                    ':postID' => $postID,
                    ':catID' => $catID
                ));
            }
        }

        //redirect to index page
        header('Location: admin.php?action=added');
        exit;

    } catch(PDOException $e) {
        echo $e->getMessage();
    }

    }
    if(isset($error)){
    foreach($error as $error){
        echo '<p class="error">'.$error.'</p>';
    }
    }
}
?>

<form action='' method='post'>

    <p><label>Title</label><br />
    <input type='text' name='postTitle' value='<?php if(isset($error)){ echo $_POST['postTitle'];}?>'></p>

    <p><label>Description</label><br />
    <textarea name='postDesc' cols='60' rows='10'><?php if(isset($error)){ echo $_POST['postDesc'];}?></textarea></p>

    <p><label>Content</label><br />
    <textarea name='postCont' cols='60' rows='10'><?php if(isset($error)){ echo $_POST['postCont'];}?></textarea></p>

    <fieldset>
        <legend>Categories</legend>
        <?php    
        $stmt2 = $db->query('SELECT catID, catTitle FROM blog_cats ORDER BY catTitle');
        while($row2 = $stmt2->fetch()){
            if(isset($_POST['catID'])){

                if(in_array($row2['catID'], $_POST['catID'])){
                   $checked="checked='checked'";
                }else{
                   $checked = null;
                }
            }
            echo "<input type='checkbox' name='catID[]' value='".$row2['catID']."' $checked> ".$row2['catTitle']."<br />";
        }
        ?>
    </fieldset>

    <p><input type='submit' name='submit' value='Submit'></p>

</form>
</div>
</body>
</html>