<?php require('includes/config.php'); ?>
<?php require 'head.php'; ?>
<body>
<?php require 'nav.php'; ?>

<div class="container">

	<div class="sixteen columns">
		</br>
	</div>

  	<div class="sixteen columns">

		<?php
		$stmt = $db->prepare('SELECT postID, postTitle, postCont, postDate FROM blog_posts WHERE postID = :postID');
		$stmt->execute(array(':postID' => $_GET['id']));
		$row = $stmt->fetch();

		if($row['postID'] == ''){
		    header('Location: blog.php');
		    exit;
		}

		echo '<div>';
		    echo '<h2>'.$row['postTitle'].'</h2>';
		    echo '<p>Posted on '.date('jS M Y', strtotime($row['postDate'])).'</p>';
		    echo '<p>'.$row['postCont'].'</p>';                
		echo '</div>';
		?>

		<br />
		<br />

		<div id="disqus_thread"></div>
			<script type="text/javascript">
			    /* * * CONFIGURATION VARIABLES * * */
			    var disqus_shortname = 'atkelsall';
			    
			    /* * * DON'T EDIT BELOW THIS LINE * * */
			    (function() {
			        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
			        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
			        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
			    })();
			</script>
			<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>

	</div>
</div>
</body>
</html>
