<h3 id="accentColour">AIDa Assignment Website</h3>
    <a href="http://atkelsall.co.uk/AIDa/index.php" target="_blank">Visit Site</a>
    <p>
      A website made for a university assignment to showcase my use of web technologies.
      <br />
      The assignment brief primarily focused on back-end development, utilising technologies such as: PHP, XML, JSON and AJAX.
      <br />
      The use of PHP was undertaken with an object oriented approach as well as being used as an introduction to MVC systems, the system used was CodeIgniter.
    </p>
    <div class="jcarousel-wrapper">
		<div class="jcarousel">
			<ul>
				<li>
					<img src="images/Screenshots/AIDa1.png" />
				</li>
				<li>
					<img src="images/Screenshots/AIDa2.png" />
				</li>
				<li>
					<img src="images/Screenshots/AIDa3.png" />
				</li>
			</ul>
		</div>
		<a href="#" class="jcarousel-control-prev">&lsaquo;</a>
		<a href="#" class="jcarousel-control-next">&rsaquo;</a>
		<p class="jcarousel-pagination"> 
		</p>
	</div>