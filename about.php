<?php require 'head.php'; ?>
<body>
<?php require 'nav.php'; ?>

<div class="whiteBackground">
<div class="container">
	<div class="sixteen columns">
		<br />
    <br />
	</div>

  <div class="three columns" id="centered">
    <img class="profileImage" src="images/profileImage.png" />
    <br />
    <script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
    <script type="IN/MemberProfile" data-id="https://www.linkedin.com/in/atkelsall" data-format="hover" data-text="Alex Kelsall"></script>
  </div>

  <div id="paddedText" class="twelve columns">
    <p>
        I am a final year undergraduate student at Leeds Beckett University studying BSc. (Hons) Computing.
        And I have chosen to specialise in software development, both web based development and application based development.
        <br />
        <br />
        I have 5 years experience in software development, starting with 2 years of Pascal Development whilst at College followed by 3 years experience with C# and Java at University.
        <br />
        My web development experience includes 3 years of development whilst at University, as well as personal projects for local clients and businesses.
    </p>
    <br />
  </div>
</div>
</div>

<div class="container">
  <div class="sixteen columns">
    <br />
    <h3>Skills</h3>
  </div>
      
      <div class="one-third column"> <!-- First Skills Column -->
        <h5 id="accentColour">C++</h5>
          <p>
            I am currently developing a drum machine based music application, this is the first application I have developed in C++
          </p>

        <h5 id="accentColour">PHP</h5>
          <p>
            PHP is probably the most common web language I develop in, both in raw form and with an MVC framework
          </p>
          <br />

        <h5 id="accentColour">SQL</h5>
          <p>
            My experience with databases is in SQL, developing tables primarily for use with web projects
          </p>
          <br />
          <br />
          <br />
          <br />

        <h5 id="accentColour">Environments</h5>
          <p>
            The environments I use most frequenctly whilst developing are Visual Studio, Eclipse and Sublime Text
          </p>
      </div>

      <div class="one-third column"> <!-- Second Skills Column -->
      <h5 id="accentColour">C#</h5>
        <p>
          I have developed a few applications in C# whilst at university, ranging from work with text files to work with images and graphics
        </p>

      <h5 id="accentColour">HTML5 / CSS3</h5>
        <p>
          I have strong working knowledge of HTML and CSS, learned through developing multiple web projects
        </p>
        <br />

      <h5 id="accentColour">AJAX / JSON</h5>
        <p>
          My experience with JSON is using APIs and data systems from other websites. I have used AJAX to create dynamic web page content and incorporated it with JSON whilst at university
        </p>
        <br />
        <br />

      <h5 id="accentColour">Hardware</h5>
        <p>
          I have built a number of desktop computers, for personal use and also for family and friends.
        </p>
    </div>

    <div class="one-third column"> <!-- Third Skills Column -->
      <h5 id="accentColour">Java</h5>
        <p>
          I have developed a few android applications in Java whilst at university
        </p>
        <br />
        <br />

      <h5 id="accentColour">Javascript (jQuery)</h5>
        <p>
          My javascript experience is primarily web based; utilising the jQuery library, applying front-end features to my web projects
        </p>

      <h5 id="accentColour">MVC / Version Control</h5>
        <p>
          I have a working understanding of how an MVC system works, my experience is primarily with the PHP MVC system CodeIgniter. 
          <br />
          Throughout university I have used Git version control for a range of projects, using Bitbucket and Sourcetree.
        </p>

        <h5 id="accentColour">Other</h5>
          <p>
            I am familiar with the use of binary and assembly language and I have experience using Windows, Linux and Mac operating systems.
          </p>      
    </div>
</div>  
</body>  
</html>