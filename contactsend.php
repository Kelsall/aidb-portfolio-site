<?php
 
if(isset($_POST['email'])) {
  $emailTo = "akelsall22@gmail.com";
  $emailSubject = "Query from A T Kelsall";
 
  function died($error) {
    // your error code can go here
    echo "The following error(s) were found with the form you submitted. <br /><br />";
    echo $error."<br /><br />"; 
    echo 'Please <a href="contact.php">Return</a> and fix these errors.<br /><br />'; 
    die(); 
    }
 
  // validation expected data exists
  if(!isset($_POST['name']) || !isset($_POST['email']) || !isset($_POST['query'])){
    died('Form incomplete!');       
  }
 
  $name = $_POST['name'];
  $emailFrom = $_POST['email'];
  $query = $_POST['query'];
 
  $errorMessage = "";
 
  $emailExp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
  $stringExp = "/^[A-Za-z .'-]+$/";

  if(!preg_match($emailExp,$emailFrom)){
    $errorMessage .= 'The Email Address you entered does not appear to be valid.<br />';
  }
 
  if(!preg_match($stringExp,$name)){
    $errorMessage .= 'The Name you entered does not appear to be valid.<br />';
  }
 
  if(strlen($query) < 2) {
    $errorMessage .= 'The query you entered does not appear to be valid.<br />';
  }
 
  if(strlen($errorMessage) > 0) {
    died($errorMessage);
  }
 
  $emailMessage = "You've received a query from A T Kelsall.\n\n";
 
  function clean_string($string) {
    $bad = array("content-type","bcc:","to:","cc:","href");
    return str_replace($bad,"",$string);
    }
 
  $emailMessage .= "Name: ".clean_string($name)."\n";
  $emailMessage .= "Email: ".clean_string($emailFrom)."\n";
  $emailMessage .= "Query: ".clean_string($query)."\n";
 
  // create email headers
  $headers = 'From: '.$emailFrom."\r\n".
  'Reply-To: '.$emailFrom."\r\n" .
  'X-Mailer: PHP/' . phpversion();
  @mail($emailTo, $emailSubject, $emailMessage, $headers);  
?>
 
Thank you, your query has been sent. We will get back to you as soon as possible. <a href="contact.html">Return</a>
 
<?php
}
?>