<h3 id="accentColour">P A Kelsall</h3>
    <a href="http://www.pakelsall.co.uk" target="_blank">Visit Site</a>
    <p>A portfolio website for the local plumbing and heating engineer, P A Kelsall.</p>
    <p>One of the first websites that I have developed. The website was developed using the Bootstrap framework.</p>
    <div class="jcarousel-wrapper">
		<div class="jcarousel">
			<ul>
				<li>
					<img src="images/Screenshots/pa1.png" />
				</li>
				<li>
					<img src="images/Screenshots/pa2.png" />
				</li>
			</ul>
		</div>
		<a href="#" class="jcarousel-control-prev">&lsaquo;</a>
		<a href="#" class="jcarousel-control-next">&rsaquo;</a>
		<p class="jcarousel-pagination"> 
		</p>
	</div>