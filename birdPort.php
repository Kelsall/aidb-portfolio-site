<h3 id="accentColour">Bird App</h3>
    <p>The first app that I made was a game based android app, with on tap commands.</p>
    <p>The app features a sprite in the form of a bird and the user is required to tap the sprite as many times as possible within a certain time scale.
    	The sprite is generated in random places on the screen, with random directions and speeds.</p>