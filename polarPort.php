<h3 id="accentColour">Polar Cycle System</h3>
	<p>Currently under development</p>
    <p>A windows form application developed in c#, that reads a text file (HRM) from hardware and displays the raw data, as well as processing the data to find useful information such as averages.</p>
    <p>The data is also displayed in graphs, with functionality to grab and display useful information from the graph plots.</p>