<h3 id="accentColour">Fractal</h3>
    <p>A windows form application, converted from java to c# as an introduction to using the c# programming language.</p>
    <p>The application is a graphical based program, displaying a mandelbrot fractal as well as offering functionality such as zooming and also colour changes.</p>