<?php require 'head.php'; ?>
<body>
<?php require 'nav.php'; ?>

<div class="container">

	<div class="sixteen columns">
		<br />
    <br />
	</div>

  <div id="contact" class="eight columns">
    <h3 id="accentColour">Get in contact</h3>
    <p>
      Enter your details &amp; message below and I'll get back to you.
    </p>
    <form name="contactform" method="post" action="contactsend.php">
              Name:  <input type="text" name="name" maxlength="80" size="30" placeholder="Enter your name"><br>
              Email: <input type="text" name="email" maxlength="80" size="30" placeholder="Enter your email"><br>
              Message: <textarea  name="query" maxlength="1000" cols="25" rows="6" placeholder="Enter your message"></textarea><br>
              <button class="accentButton">Send</button>
    </form>
  </div>  
</body>  
</html>