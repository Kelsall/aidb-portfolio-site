<?php require 'head.php'; ?>
<body>
<?php require 'nav.php'; ?>

<div class="container">

	<div class="sixteen columns">
		<br />
    <br />
	</div>

  <div class="sixteen columns">
  	<h3>Portfolio</h3>
  </div>

  <div class="sixteen columns">
    <h4 id="accentColour">Web Projects</h4>
    </br>
    <ul class="imgFade">
          <li>
            <a href="#" data-reveal-id="modalSav">
            <img src="images/savage300.jpg" />
            <span class="text-content"><span>Portfolio website for local photographer</span></span>
            </a>
          </li>
          <li>
            <a href="#" data-reveal-id="modalAIDa">
            <img src="images/AIDa300.jpg" />
            <span class="text-content"><span>University Assignment</span></span>
            </a>
          </li>
          <li>
            <a href="#" data-reveal-id="modalPA">
            <img src="images/pa300.jpg" />
            <span class="text-content"><span>Portfolio website for local plumbing and heating engineer</span></span>
            </a>
          </li> 
    </ul>
    <ul class="imgFade">
          <li>
            <a href="#" data-reveal-id="modalTent">
            <img src="images/tent300.jpg" />
            <span class="text-content"><span>Gallery website and blog for Yorkshire Tent Hire Company</span></span>
            </a>
          </li>
          <li>
            <a href="#" data-reveal-id="modalDrum">
            <img src="images/drum300.jpg" />
            <span class="text-content"><span>Website to showcase Drum Machine<br />(under construction)</span></span>
            </a>
          </li>
          <li>
            <img src="images/300thumb.jpg" />
            <span class="text-content"><span>Web Project</span></span>
          </li>
    </ul>
    <br />
  </div>

  <div class="sixteen columns">
    <h4 id="accentColour">Software Projects</h4>
    </br>
    <ul class="imgFade">
      <li>
        <a href ="#" data-reveal-id="modalDrumApp">
        <img src="images/drum300.jpg" />
        <span class="text-content"><span>Drum Machine</span></span>
        </a>
      </li>
      <li>
        <a href ="#" data-reveal-id="modalFractal">
        <img src="images/fractal300.png" />
        <span class="text-content"><span>Fractal</span></span>
        </a>
      </li>    
      <li>
        <a href ="#" data-reveal-id="modalPolar">
        <img src="images/polar300.jpg" />
        <span class="text-content"><span>Polar Cycle Software</span></span>
        </a>
      </li>
    </ul>
    <ul class="imgFade">
      <li>
        <a href ="#" data-reveal-id="modalBird">
        <img src="images/bird300.jpg" />
        <span class="text-content"><span>Bird App</span></span>
        </a>
      </li>
      <li>
        <img src="images/300thumb.jpg" />
        <span class="text-content"><span>Software Project</span></span>
      </li>    
      <li>
        <img src="images/300thumb.jpg" />
        <span class="text-content"><span>Software Project</span></span>
      </li>
    </ul>
    <br />
  </div>
</div>

<div class="whiteBackground">
  <div class="container">
    <div class="sixteen columns">
      <br />
      <h3>Example Code</h3>
      <br />
    </div>

    <div class="eight columns">
      <h4 id="accentColour">BitBucket</h4>
        <p>
          git version control of University projects
          <br />
          <a href="http://bitbucket.org/Kelsall" target="_blank">View</a>
        </p>
      <br />
    </div>

    <div class="eight columns">
      <h4 id="accentColour">StackOverflow</h4>
        <p>
          Interaction with the StackOverflow community
          <br />
          <a href="http://stackoverflow.com/users/4500698/kelsall" target="_blank">View</a>
        </p>
      <br />
    </div>
  </div>  
</div>

<!-- Modal -->
  <div id="modalSav" class="reveal-modal">
    <?php
      require 'savPort.php';
    ?>
    <a class="close-reveal-modal">&#215;</a>
  </div>

  <div id="modalTent" class="reveal-modal">
    <?php
      require 'tentPort.php';
    ?>
    <a class="close-reveal-modal">&#215;</a>
  </div>

  <div id="modalPA" class="reveal-modal">
    <?php
      require 'paPort.php';
    ?>
    <a class="close-reveal-modal">&#215;</a>
  </div> 

  <div id="modalAIDa" class="reveal-modal">
    <?php
      require 'AIDaPort.php';
    ?>
    <a class="close-reveal-modal">&#215;</a>
  </div>

  <div id="modalDrum" class="reveal-modal">
    <?php
      require 'drumPort.php';
    ?>
    <a class="close-reveal-modal">&#215;</a>
  </div>

  <div id="modalDrumApp" class="reveal-modal">
    <?php
      require 'drumAppPort.php';
    ?>
    <a class="close-reveal-modal">&#215;</a>
  </div>

  <div id="modalFractal" class="reveal-modal">
    <?php
      require 'fractalPort.php';
    ?>
    <a class="close-reveal-modal">&#215;</a>
  </div>

  <div id="modalPolar" class="reveal-modal">
    <?php
      require 'polarPort.php';
    ?>
    <a class="close-reveal-modal">&#215;</a>
  </div>

  <div id="modalBird" class="reveal-modal">
    <?php
      require 'birdPort.php';
    ?>
    <a class="close-reveal-modal">&#215;</a>
  </div>

</body>
</html>  