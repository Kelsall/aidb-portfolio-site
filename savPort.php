<h3 id="accentColour">Matthew Savage Photography</h3>
    <a href="http://atkelsall.co.uk/MatthewSavagePhotography/Home.php" target="_blank">Visit Site</a>
    <p>Portfolio website for local photographer, Matthew Savage</p>
    <p>Client based website mainly based around a front end design, utilising CSS and jQuery</p>
    <div class="jcarousel-wrapper">
		<div class="jcarousel">
			<ul>
				<li>
					<img src="images/Screenshots/sav1.png" />
				</li>
				<li>
					<img src="images/Screenshots/sav2.png" />
				</li>
			</ul>
		</div>
		<a href="#" class="jcarousel-control-prev">&lsaquo;</a>
		<a href="#" class="jcarousel-control-next">&rsaquo;</a>
		<p class="jcarousel-pagination"> 
		</p>
	</div>