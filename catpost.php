<?php
    require('../includes/config.php');
    $stmt = $db->prepare('SELECT catID,catTitle FROM blog_cats WHERE catSlug = :catSlug');
    $stmt->execute(array(':catSlug' => $_GET['id']));
    $row = $stmt->fetch();

    //if post does not exists redirect user.
    if($row['catID'] == ''){
        header('Location: ./');
        exit;
    }
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

    <!-- Basic Page Needs
  ================================================== -->
    <meta charset="utf-8">
    <title>A T Kelsall</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Metas
  ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
  ================================================== -->
  <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="../css/base.css">
    <link rel="stylesheet" href="../css/skeleton.css">
    <link rel="stylesheet" href="../css/layout.css">
    <link rel="stylesheet" href="../js/jqueryLibrary/jquery-ui.min.css">

    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="../images/favicon.ico">
    <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-touch-icon-114x114.png">

    <!-- Script
    ================================================== -->
    <script src="../js/jqueryLibrary/external/jquery/jquery.js"></script>
    <script src="../js/jqueryLibrary/jquery-ui.min.js"></script>
    <script language="JavaScript" type="text/javascript">
    function delpost(id, title)
    {
      if (confirm("Are you sure you want to delete '" + title + "'"))
      {
          window.location.href = 'admin.php?delpost=' + id;
      }
    }
    </script>
</head>

<body>
<header>
        <h1 id="siteHeader">A T Kelsall</h1>
        <h4 id="accentColour">Software and Web Development</h4>
</header>

<nav>
    <ul>
      <li>
        <a href="../index.php">Home</a>
      </li>
      <li>
        <a href="../about.php">About</a>
      </li>
      <li>
        <a href="../portfolio.php">Portfolio</a>
      </li>
      <li>
        <a href="../blog.php">Blog</a>
      </li>
      <li>
        <a href="../contact.php">Contact</a>
      </li>
    </ul>
</nav>

<div class="container">
    
    <?php
    //admin menu
    include 'menu.php';
    ?>

    <h3>Blog Posts</h3>
    <p>Posts in <?php echo $row['catTitle'];?></p>

    <?php    
        try {

            $stmt = $db->prepare('
                SELECT 
                    blog_posts.postID, blog_posts.postTitle, blog_posts.postDesc, blog_posts.postDate 
                FROM 
                    blog_posts,
                    blog_post_cats
                WHERE
                     blog_posts.postID = blog_post_cats.postID
                     AND blog_post_cats.catID = :catID
                ORDER BY 
                    postID DESC
                ');
            $stmt->execute(array(':catID' => $row['catID']));
            while($row = $stmt->fetch()){
                
                echo '<div>';
                    echo '<h1><a href="'.$row['postID'].'">'.$row['postTitle'].'</a></h1>';
                    echo '<p>Posted on '.date('jS M Y H:i:s', strtotime($row['postDate'])).' in ';

                        $stmt2 = $db->prepare('SELECT catTitle, catSlug    FROM blog_cats, blog_post_cats WHERE blog_cats.catID = blog_post_cats.catID AND blog_post_cats.postID = :postID');
                        $stmt2->execute(array(':postID' => $row['postID']));

                        $catRow = $stmt2->fetchAll(PDO::FETCH_ASSOC);

                        $links = array();
                        foreach ($catRow as $cat)
                        {
                            $links[] = "<a href='c-".$cat['catSlug']."'>".$cat['catTitle']."</a>";
                        }
                        echo implode(", ", $links);

                    echo '</p>';
                    echo '<p>'.$row['postDesc'].'</p>';                
                    echo '<p><a href="'.$row['postID'].'">Read More</a></p>';                
                echo '</div>';

            }

        } catch(PDOException $e) {
            echo $e->getMessage();
        }

        ?>

</div>
</body>
</html>

