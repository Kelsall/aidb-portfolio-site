<?php require 'head.php'; ?>
<body>
<?php require 'nav.php'; ?>

<div class="whiteBackground">
  <div class="container">

  	<div class="sixteen columns">
  		<br />
      <br />
  	</div>

    <div class="one-third column">
      <img id="centeredImage" class="profileImage" src="images/profileImage.png" />
    </div>

    <div class="two-thirds column">
    	<p>
          Alex Kelsall is a software and web developer, developing windows applications, mobile apps and web projects. <br />
          Development projects range from windows applications in C++ and C#, android applications with Java and also web projects utilising a range of web technologies, such as; PHP, HTML and jQuery. 
      </p>
      <form action="about.php">
        <button class="accentButton">Find out more</button>
      </form>
    </div>

    <div class="sixteen columns">
      <br />
    </div>

  </div>
</div>

<div class="container">
  <div class="sixteen columns">
    <br />
  	<h3 id="accentColour">Portfolio</h3>
    <ul class="imgFade">
          <li>
            <a href="#" data-reveal-id="modalSav">
              <img src="images/savage300.jpg" />
              <span class="text-content"><span>Portfolio website for local photographer</span></span>
            </a>
          </li>
          <li>
            <a href="#" data-reveal-id="modalTent">
            <img src="images/tent300.jpg" />
            <span class="text-content"><span>Gallery website and blog for Yorkshire Tent Hire Company</span></span>
            </a>
          </li>
          <li>
            <a href="#" data-reveal-id="modalPA">
            <img src="images/pa300.jpg" />
            <span class="text-content"><span>Portfolio website for local plumbing and heating engineer</span></span>
            </a>
          </li> 
    </ul>
    <ul class="imgFade">
          <li>
            <a href ="#" data-reveal-id="modalDrumApp">
            <img src="images/drum300.jpg" />
            <span class="text-content"><span>Drum Machine</span></span>
            </a>
          </li>
          <li>
            <a href ="#" data-reveal-id="modalFractal">
            <img src="images/fractal300.png" />
            <span class="text-content"><span>Fractal</span></span>
            </a>
          </li>    
          <li>
            <a href ="#" data-reveal-id="modalPolar">
            <img src="images/polar300.jpg" />
            <span class="text-content"><span>Polar Cycle Software</span></span>
            </a>
          </li>
    </ul>
    <form id="centered" action="portfolio.php">
      <button class="accentButton">View more</button>
    </form>
    <br />
  </div>
  </div>

  <!-- Modal -->
  <div id="modalSav" class="reveal-modal">
    <?php
      require 'savPort.php';
    ?>
    <a class="close-reveal-modal">&#215;</a>
  </div>

  <div id="modalTent" class="reveal-modal">
    <?php
      require 'tentPort.php';
    ?>
    <a class="close-reveal-modal">&#215;</a>
  </div>

  <div id="modalPA" class="reveal-modal">
    <?php
      require 'paPort.php';
    ?>
    <a class="close-reveal-modal">&#215;</a>
  </div>  

  <div id="modalDrumApp" class="reveal-modal">
    <?php
      require 'drumAppPort.php';
    ?>
    <a class="close-reveal-modal">&#215;</a>
  </div>

  <div id="modalFractal" class="reveal-modal">
    <?php
      require 'fractalPort.php';
    ?>
    <a class="close-reveal-modal">&#215;</a>
  </div>

  <div id="modalPolar" class="reveal-modal">
    <?php
      require 'polarPort.php';
    ?>
    <a class="close-reveal-modal">&#215;</a>
  </div>

</body>
</html>  