<?php require('includes/config.php'); ?>
<?php require 'head.php'; ?>
<body>
<?php require 'nav.php'; ?>

<div class="container">

	<div class="fourteen columns">
		<br />
        <br />
	</div>
    <div class="two columns">
        <br/>
        <a href="admin/login.php">Log In</a>
    </div>

  <div class="sixteen columns">
    <?php
    try 
    {
        $stmt = $db->query('SELECT postID, postTitle, postDesc, postDate FROM blog_posts ORDER BY postID DESC');
        while($row = $stmt->fetch()){
            
            echo '<div>';
                echo '<h3><a href="viewpost.php?id='.$row['postID'].'">'.$row['postTitle'].'</a></h3>';
                echo '<p>Posted on '.date('jS M Y H:i:s', strtotime($row['postDate'])).' in ';
                $stmt2 = $db->prepare('SELECT catTitle, catSlug    FROM blog_cats, blog_post_cats WHERE blog_cats.catID = blog_post_cats.catID AND blog_post_cats.postID = :postID');
                $stmt2->execute(array(':postID' => $row['postID']));
                $catRow = $stmt2->fetchAll(PDO::FETCH_ASSOC);
                $links = array();
                foreach ($catRow as $cat){
                     $links[] = "<a href='c-".$cat['catSlug']."'>".$cat['catTitle']."</a>";
                }
                echo implode(", ", $links);
                echo '</p>';
                echo '<p>'.$row['postDesc'].'</p>';                
                echo '<p><a href="viewpost.php?id='.$row['postID'].'">Read More</a></p>';
                echo '<hr>';                
            echo '</div>';

        }

    }
    catch(PDOException $e)
    {
        echo $e->getMessage();
    }
	?>

  </div>
  
  </div>  
</body>  
</html>